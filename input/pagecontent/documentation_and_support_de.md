> **For the english version please click [here](documentation_and_support_en.html).**

### Die ELGA-Terminologien

#### Codesysteme
In ELGA wird eine Vielzahl unterschiedlicher Codesysteme mit unterschiedlicher Struktur von verschiedenen internationalen, nationalen und lokalen Quellen verwendet. Über den Terminologieserver werden diese in eine möglichst einheitliche Form gebracht.

Die Aktualisierung von Codesystemen und deren Zeitpunkt oder Häufigkeit liegt in der Verantwortung der Ersteller und damit meist außerhalb des Einflussbereiches von ELGA.

Ein Codesystem wird durch eine OID eindeutig identifiziert. Die Bedeutung eines Codes kann nur gemeinsam mit der OID des Codesystems entschlüsselt werden.

Ein Grundsatz der Terminologie-Entwicklung ist, dass Codes nicht gelöscht und auch die Bedeutung eines Codes nicht verändert werden dürfen [^1]. Solche Änderungen erzeugen eine Inkonsistenz bei der Verwendung von älteren codierten Daten mit der aktuellen Version des Codesystems. Üblicherweise bekommen solche „inkompatiblen“ Versionsstände des Codesystems unterschiedliche OIDs um sie auseinanderzuhalten (z.B. die jährlichen Versionen des ICD-10). Nicht alle Codesysteme halten sich in der Praxis an diese Vorgabe. Bei der Übernahme von Aktualisierungen von Codesystemen ist daher immer mit Bedacht vorzugehen.

#### Value Set
Wo immer durch ELGA CDA Implementierungsleitfäden eine Werteauswahl getroffen werden kann, wird ein passendes Value Set definiert und mit seinem eindeutigen Namen angegeben. ELGA Value Sets beziehen sich immer [^2] auf eine bestimmte „Version“ eines Codesystems oder auch mehrerer Codesysteme, was über die OID der Codesysteme erkenntlich ist. Dadurch werden die Value Sets unabhängig von der Veränderung der Codesysteme.

Sämtliche in den Implementierungsleitfäden verwendeten Value Sets werden am [österreichischen e-Health Terminologie-Browser](https://termgit.elga.gv.at/) publiziert.

Value Sets sind nicht nur durch einen eindeutigen Namen, sondern auch durch eine OID und eine Versionsnummer gekennzeichnet, dazu wird ein Gültigkeitsdatum („gültig ab“) angegeben. Darüber hinaus können Value Sets eine Reihenfolge und eine Baumstruktur (Hierarchie) enthalten.

Value Sets sind so lange gültig, bis das Gültigkeitsdatum einer neueren Version dieses Value Sets erreicht wird – dann gilt die neuere Version [^3]. Solange keine neuere Version vorhanden ist, bleibt ein Value Set gültig (siehe auch Kapitel [Value Set Binding](#value-set-binding)).

Value Sets benötigen keine Status-Information für die einzelnen Codes, weil per Definition nur „gültige und verwendbare“ Codes in einem Value Set enthalten sind.
Wenn Codes, ab einem bestimmten Gültigkeitsdatum nicht mehr verwendet werden dürfen, wird eine neue Version des Value Sets erzeugt, aus dem die Codes entfernt sind. Die neue Version des Value Sets erhält dann das neue „gültig ab“-Datum.

##### Value Set Binding
Für jedes Value Set ist ein Zeitpunkt angegeben, an dem es Gültigkeit erlangt („gültig ab“ = ValueSet.date). Dies ist besonders wichtig für Value Sets, die schon vor ihrem Inkrafttreten veröffentlicht werden.

Für ELGA gilt grundsätzlich eine DYNAMISCHE Bindung an Value Sets. Das bedeutet, dass immer die aktuell am Terminologieserver publizierte Version eines Value Sets anzuwenden ist. (Das Setzen des entsprechenden Schlüsselworts `DYNAMIC` ist daher in den Leitfäden nicht erforderlich).

Value Sets können auch STATISCH an ein Code-Element gebunden werden. Dies wird durch die Angabe des Value Sets mit Name, OID, Version und „gültig ab“-Datum (ValueSet.date) sowie dem Schlüsselwort `STATIC` gekennzeichnet.

##### Änderbarkeit von Value Sets
Inhalte von Value Sets können sich ändern, der Name und die OID eines Value Sets bleiben aber gleich. Bei neuen Versionen werden Versionsnummer, Änderungsdatum und „gültig ab“-Datum (ValueSet.date) angegeben. Damit kann die Gültigkeit zu einer bestimmten Zeit rekonstruiert werden.

In Ausnahmen kann bei der Definition eines Value Sets (im Leitfaden) angegeben werden, dass es nicht geändert oder versioniert werden darf (Property „Immutability“ = FHIR Value Set "immutable").

Wie häufig Value Sets geändert werden, hängt von der jeweiligen Anwendung ab. Value Sets, die für „strukturelle“ Elemente benötigt werden (z.B. im CDA-Header, XDS-Metadaten) werden sich selten ändern. „Inhaltliche“ Value Sets (z.B. für Arzneimittellisten, Laboranalysen, Diagnosen-Codierungen) werden sich entsprechend häufig ändern müssen, hier ist mit jährlichen bis wöchentlichen Änderungen zu rechnen.

#### Kommunikation der Veränderung von Terminologien
Alle Änderungen von Terminologien sind am Terminologieserver zentral abrufbar. Prüfungen auf geänderte Terminologien sollten mindestens **wöchentlich** erfolgen.

#### Anforderung von Erweiterungen oder Korrekturen von Terminologien
Alle Fragen zu ELGA-relevanten Terminologien können an [cda@elga.gv.at](mailto:cda@elga.gv.at) gemeldet werden. Von dieser Adresse aus wird die Bearbeitung zentral koordiniert.

Codesysteme können nur dann korrigiert werden, wenn (1) ein technisches Problem vorliegt oder (2) es sich um eine in Auftrag der ELGA GmbH entwickeltes Codesystem handelt.

Von und für ELGA definierte Value Sets können nach fachlicher Prüfung durch die Inhaltsverwalter korrigiert werden. Bei ELGA Value Sets, die einem regulären Wartungs- und Updatezyklus unterliegen, können Erweiterungen und Änderungen durch den zuständigen Inhaltsverwalter durchgeführt werden (z.B. ELGA Laborparameter, ASP-Liste). Bei anderen inhaltlichen Korrekturen von ELGA Value Sets muss gegebenenfalls die fachliche Entscheidung der Arbeitsgruppe [^4] eingeholt werden, die den entsprechenden Leitfaden entwickelt hat, in dessen Rahmen das Value Set definiert wurde.

Terminologiewartung --> wird bereits oben beschrieben

### Installationsanleitung
in Bearbeitung

### Architektur
in Bearbeitung

### GitLab-Projektübersicht

Der Inhalt des [österreichischen e-Health Terminologie-Browsers](https://termgit.elga.gv.at/) basieren auf den folgenden zwei GitLab-Projekten:

| Projektname | Beschreibung | Repository-URL | GitLab-Projekt-ID | GitLab-Pages-URL |
| --- | --- | --- | --- | --- |
| TerminoloGit | [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) | Repository für publizierte, aktuelle Terminologien (Downloadformate).<br/><br/>*Hinweis:* Unter dem angegebenen GitLab-Pages-URL werden die Inhalte der letzten erfolgreichen Pipeline eines beliebigen Git-Branches dieses Repositories dargestellt. | 33179072 | [https://elga-gmbh.gitlab.io/termgit](https://elga-gmbh.gitlab.io/termgit) |
| TerminoloGit HTML | [https://gitlab.com/elga-gmbh/termgit-html](https://gitlab.com/elga-gmbh/termgit-html) | Repository für die statischen HTML-Seiten, die vom HL7® FHIR® IG Publisher auf Basis des `main`-Branches von [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) erstellt wurden. | 33179017 | **[https://termgit.elga.gv.at](https://termgit.elga.gv.at)** |

### Freigabeprozess
1. Status „Entwurf“: Nur bestimmte Benutzer dürfen Terminologien anlegen, ändern bzw. importieren. Jede Terminologie hat dafür einen Terminologie-Verantwortlichen, Terminologie-Administratoren können alle Terminologien verändern. Alle neu angelegten oder neu importierten Terminologien bzw. Terminologie-Versionen tragen den Status „Entwurf“, der sie nur für den zugeordneten Terminologie-Verantwortlichen und alle Terminologie-Administratoren sichtbar macht.
2. Status „zur Freigabe“: Der Terminologie-Verantwortliche gibt die Terminologie-Version inhaltlich frei, die Terminologie nimmt dadurch diesen neuen Status an (weiterhin nur für zugeordneten Terminologie-Verantwortlichen und Terminologie-Administratoren sichtbar).
3. Status „freigegeben“: Die erstellten bzw. geänderten Terminologien sind nach einer Freigabe durch einen Terminologie-Administrator auch für Terminologie-Consumer verfügbar.
4. Status „ungültig“: Diese Terminologien dürfen nicht (mehr) verwendet werden. Dies kann zwei Auslöser haben:
   1. Der erstellte Entwurf wurde vom Terminologie-Administrator abgelehnt.
   2. Der zugeordnete Terminologie-Verantwortliche oder ein Terminologie-Administrator zieht die Version zurück und setzt sie manuell auf ungültig.


![Freigabeprozess](Freigabeprozess.png "Freigabeprozess"){: style="width: 100%"}

### Publikationsprozess für Terminologien externer Herausgeber

Für die Publikation von Terminologien, die von ELGA-externen Terminologie-Developern für den österreichischen e-Health Terminologieserver erstellt wurden, ist folgender Prozess definiert.

#### Voraussetzungen

- Basiskenntnisse von Git bzw. GitLab werden als bekannt vorausgesetzt.
- Die gewünschte Terminologie unterliegt keinen externen Lizenzbestimmungen und besitzt bereits eine eigene OID. Falls noch keine OID für die Terminologie vorliegt, kann diese über die [OID Plattform](https://www.gesundheit.gv.at/OID_Frontend/application.htm?section=5) beantragt werden.

#### Vorgehensweise

1. Kontaktaufnahme mit der ELGA GmbH über die E-Mail Adresse [cda@elga.gv.at](mailto:cda@elga.gv.at) mit **[ausgefülltem Antragsformular](files/Applicationform_external_terminology_developer_de.pdf)**:
   1. Name der Terminologie
   2. OID der Terminologie
   3. Codesystem/Value Set
      1. Falls Value Set: Bekanntgabe des/der dazu gehörigen Codesystems/Codesysteme
   4. Beschreibung zur Terminologie (DE und EN)
   5. Verantwortliche Person und Organisation für die Terminologie
      1. Name (Organisation)
      2. E-Mail Adresse (Organisation)
      3. Name (verantwortliche Person)
      4. E-Mail Adresse (verantwortliche Person)
   6. GitLab.com-User, die die Terminologie in das GitLab-Projekt [TerminoloGit](https://gitlab.com/elga-gmbh/termgit) einbringen soll. Die Bekanntgabe mehrerer GitLab.com-User ist möglich.
2. Das ausgefüllte Formular wird von ELGA GmbH technisch und rechtlich geprüft.
3. Nach erfolgreicher Prüfung erhalten die vom Antragsteller genannten GitLab-User "Developer"-Rechte im GitLab-Projekt [TerminoloGit](https://gitlab.com/elga-gmbh/termgit). Damit kann der User einen eigenen Branch erstellen, darf aber ohne Freigabe durch einen User mit "Maintainer"-Rechten nicht in den Default Branch des Projekts mergen.
4. Für das initiale Erstellen der Importdatei werden **Importvorlagen** für **[Codesystem](files/cs_import_template.csv)** bzw. **[Value Set](files/vs_import_template.csv)** in Form von CSV-Dateien zur Verfügung gestellt. Die benötigten Attribute sind im Abschnitt [Proprietäres CSV](#proprietäres-csv) beschrieben.
   1. Das Hochladen in das GitLab-Projekt kann über verschiedenen Wege durchgeführt werden (z.B. GitLab WebIDE oder Software wie z.B. Sourcetree). Für Fragen und Unterstützung kann sich der externe Terminologie-Developer an das TerminoloGit-Team wenden ([cda@elga.gv.at](mailto:cda@elga.gv.at)).
   2. Pro Branch sollten thematisch zusammenhängende Terminologien bearbeitet werden, damit die Übersicht gewahrt werden kann.
   3. Die Commits müssen der Git-Governance entsprechen: [https://cbea.ms/git-commit/](https://cbea.ms/git-commit/)
   4. Terminologien von externen Terminologie-Developern werden unter einer eigenen Kategorie benannt nach dem Organisationsnamen gesammelt.
5. Nach Fertigstellung der Terminologie wird ein Merge Request erstellt, damit der Branch in den Default Branch überführt werden kann.
   1. Der technische Review wird von der ELGA GmbH durchgeführt.
   2. Wenn die technischen Voraussetzungen erfüllt sind, wird der Branch in den Default Branch überführt.
6. Nachdem erfolgreichen Durchlauf der Pipelines wird kontrolliert, ob die gewünschten Terminologien korrekt und vollständig publiziert wurden.
7. Abschließend wird der Terminologie-Developer über die Publikation informiert und ein Update von der [Changeliste des Semantic Competance Centers](https://confluence.elga.gv.at/display/SCCTERM/Terminologien+-+Changelist+2022) wird veröffentlicht.

### Anwendungsfälle/Verwendung und Web Services

#### Browsing/Recherche
*Akteur:* Student

Der Browser eignet sich hervorragend für Recherchezwecke oder einfach nur zum Durchstöbern, da alle Listen in übersichtlichen HTML Seiten dargestellt werden. Der Anwender hat die Möglichkeit verschiedene Codesysteme und Value Sets einzeln in strukturierter Form abzurufen oder mit Hilfe der Suchfunktion ganz bestimmte Konzepte zu suchen. Zwischen den Codesystemen und Value Sets besteht auch immer eine Verknüpfung, um zusammen gehörige Listen leichter ausfindig zu machen.

#### Automatischer Import von Katalogen
*Akteur:* System

Damit Ihre Systeme immer auf dem aktuellsten Stand bleiben, gibt es die Möglichkeit des automatischen Imports von Katalogen. Es gibt mehrere Varianten wie Sie sicherstellen können, dass Sie immer die aktuellsten Listen in Ihrem System führen. Die hier dargestellten Varianten konzentrieren sich auf die Aktualisierung einer bestimmten Terminologie.

Grundästzlich kann mit folgend aufgebautem URL immer die aktuellste Version einer Terminologie abgerufen werden:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/<NAME_OF_TERMINOLOGY>/<NAME_OF_TERMINOLOGY><FORMAT>

Dabei entspricht
- der `<NAME_OF_TERMINOLOGY>` dem Typ der Terminologie (`CodeSystem` oder `ValueSet`) sowie Namen der Terminologie (z.B. `iso-3166-1-alpha-3`). Der Typ und der Name werden mit `-` verknüpft: `CodeSystem-iso-3166-1-alpha-3`
- das `<FORMAT>` der Dateiendung eines der angebotenen Formate (siehe [Downloadformate](#downloadformate)), z.B. `.4.fhir.xml`

Das Ergebnis sieht dann wie folgt aus:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.4.fhir.xml

Anhand eines bestimmten Formats einer Terminologie werden in den nächsten Punkten verschiedene Arten des Abrufs dargestellt:
1. Als stabiler https-Link zur Datei, ohne Versionsangaben:
   * Python:

         response = urllib2.urlopen('https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false')
   * Java:

         URL url = new URL("https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false");
         URLConnection connection = url.openConnection();
         InputStream is = connection.getInputStream();
         // .. then download the file
2. Mit Git über ssh in einem lokalen Repository und den Git-Tags für die jeweilige Terminologie:
   * Erstmaliges klonen des Repositories:

         git clone git@gitlab.com:elga-gmbh/termgit.git

   * Aktualisieren des lokalen Git-Reposistories (inkl. Git-Tags):

         git fetch --tags -f

   * Prüfen des Unterschieds zwischen dem aktuellen, lokalen Verzeichnisinhalt und dem des Git-Tags einer Terminologie nach dem Aktualisieren des Repositories:

         git log HEAD..tags/CodeSytem-iso-3166-1-alpha-3

     oder

         git log HEAD..tags/1.0.3166.1.2.3

   * Auschecken der aktuellsten Version einer Terminologie mittels des Git-Tags. **Hinweis:** Dabei wird das gesamte Repository auf den Stand des Git-Tags gesetzt. Andere Terminologien könnten einen aktuelleren Stand haben.

         git checkout tags/CodeSystem-iso-3166-1-alpha-3

     oder

         git checkout tags/1.0.3166.1.2.3

3. Mit git über ssh ohne lokalem Repository und den Git-Tags für die jeweiligen Terminologie-Order (einzelne Dateien sind hier nicht möglich):

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git CodeSystem-iso-3166-1-alpha-3:terminologies/CodeSystem-iso-3166-1-alpha-3

   oder

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git 1.0.3166.1.2.3:terminologies/CodeSystem-iso-3166-1-alpha-3

4. Mit GitLab API über REST für ein bestimmtes Format (hier .2.claml.xml; '/' in dem GitLab-Repo-Pfad müssen mit '%2f' escaped werden)

       curl https://gitlab.com/api/v4/projects/33179072/repository/files/terminologies%2fCodeSystem-iso-3166-1-alpha-3%2fCodeSystem-iso-3166-1-alpha-3.2.claml.xml?ref=main

5. Mit FHIR über REST (FHIR Server kommt mit Alpha Release im Q3 2022):
   * curl:

         curl https://<FHIR SERVER NAME>.at/CodeSystem/CodeSystem-iso-3166-1-alpha-3

   * mehr Informationen finden Sie hier: [http://www.hl7.org/fhir/overview-dev.html#Interactions](http://www.hl7.org/fhir/overview-dev.html#Interactions)
   * für Testzwecke kann beispielsweise der folgende FHIR Server verwendet werden

         curl http://hapi.fhir.org/baseR4/CodeSystem/2559241

##### Abruf von Metainformationen eines Git-Tags

Für die automatisierte Überprüfung, ob eine bestimmte Terminologie aktualisiert wurde, kann die [GitLab-API für Git-Tags](https://docs.gitlab.com/ee/api/tags.html) verwendet werden. Mit Hilfe des folgenden Befehls können die Metadaten eines Git-Tags abgerufen werden:

    curl https://gitlab.com/api/v4/projects/33179072/repository/tags/CodeSystem-iso-3166-1-alpha-3

In den Metadaten befindet sich unter anderem das Datum, an dem der Git-Tag erstellt wurde (`created_at`).

#### Manuelle Aktualisierung von Terminologien
*Akteur:* Stammdaten-/System-Admin

Neben dem automatischen Import von Katalogen, besteht ebenfalls die Möglichkeit, manuell alle von Ihnen verwendeten Terminologien zu aktualisieren. Dafür empfehlen wir die Tag-Neuigkeiten in GitLab zu aktivieren. Bei jeder Änderung erhalten Sie dann eine automatische Benachrichtigung.

![Tag-abonnieren](Tag-abonnieren.png "Tag-Neuigkeiten-abonnieren"){: style="width: 100%"}

![manuelles-mapping](automatische-benachrichtigung.png "Manuelles-Mapping"){: style="width: 100%"}

#### Validation von Codes
*Akteur:* System

Das System kann auch mit Hilfe des neuen österreichischen e-Health Terminologieservers prüfen, ob ein bestimmter Code Teil eines Codesystems oder eines Value Sets ist. Dies funktioniert mit der FHIR-Operation [`$validate-code` für Codesysteme](http://www.hl7.org/fhir/codesystem-operation-validate-code.html) bzw. [`$validate-code` für Value Sets](http://www.hl7.org/fhir/valueset-operation-validate-code.html).

![validate-code](validate-code.png "Validate_code"){: style="width: 100%"}

### Rollen
Als Grundregel ist festzuhalten, dass der Terminologieserver ein System zur Bereitstellung von Codelisten und Value Sets darstellt, wobei grundsätzlich kein Echtzeit-Zugriff bei der Verarbeitung durch die Primärsysteme (z.B.: Informationssysteme von Gesundheitsdienste-Anbietern (GDA)) vorgesehen ist. Der Terminologieserver dient derzeit nicht als Online-Ressource (für die Prüfung einzelner Werte), sondern als Quelle für neue Terminologien, die lokal abgespeichert werden sollen.
Der Terminologieserver wird von folgenden Benutzergruppen verwendet:

| Benutzergruppe | Aufgabe | GitLab.com-Rolle | Beschreibung |
| --- | --- | --- | ---|
| Terminologie-Consumer | lesender Zugriff | keine, nicht erforderlich | Als Terminologie-Consumer ist für die Anzeige und den Export von Terminologien bzw. das Überprüfen auf neue Versionen keine Selbstregistrierung notwendig. |
| Terminologie-Verantwortlicher | Terminologie-Wartung | Developer | Die Anlage oder Aktualisierung von Terminologien wird vom Terminologie-Verantwortlichen durchgeführt. Es ist nicht möglich jeder Terminologie einen verantwortlichen Benutzer zuzuordnen. |
| Terminologie-Administrator | Freigabe und Publikation | Maintainer | Bevor Terminologien öffentlich abrufbar werden, erfolgt eine Freigabe durch einen Terminologie-Administrator. |

### Versionierung

Für die Versionierung von Terminologien wie Codesysteme oder Value Sets gilt folgendes Versionierungsschema:

    MAJOR.MINOR.PATCH+YYYYMMDD

* `MAJOR` wird erhöht, wenn z.B.
    * ein Konzept hinzugefügt wird (der Wertebereich verändert sich und muss vom Implementierer in seiner Implementierung aktualisiert werden)
    * ein Konzept deprecated/veraltet gesetzt wird;
    * der DisplayName eines Konzepts geändert wird;
    * Metadaten eines Konzepts ändern sich und damit ändert sich der Sinn des Konzepts;
    * Struktur/Gruppierung (z.B. beim ValueSet) sich ändert;
* `MINOR` bleibt immer auf dem Wert 0 und wird im Rahmen der Terminologien nicht verwendet
* `PATCH` wird erhöht, wenn z.B.
    * Rechtschreibfehler oder Tippfehler in den Metadaten (nicht DisplayName oder Code) eines vorhandenen Konzepts korrigiert werden.
* `YYYYMMDD` entspricht dem Datum, an dem die Terminologie aktualisiert wurde.

Diese Versionierung wird erst nach Beendigung des Parallelbetriebs umgesetzt werden. Solange der alte Terminologieserver gewartet wird, muss sichergestellt werden, dass die Versionnummerierung am alten und neuen Terminologieserver übereinstimmen.

#### Anzeige von alten Versionen
Alte Versionen einer Terminologie können über den Tab "Previous Versions" aufgerufen werden. Die dargestellte Tabelle enthält folgende Spalten:

- `Publication Date`: Dieses Datum entspricht dem Zeitpunkt der Publikation der jeweiligen Terminologie. Durch Klick auf das Datum kann die alte Version angezeigt werden. Das Publikationsdatum kann sich vom „gültig ab“-Datum unterscheiden.
- `Current Version vs. Outdated Version`: Mittels des [W3C HTML Diff Tools](https://services.w3.org/htmldiff) können die Unterschiede zwischen der alten und aktuellen Version der Terminologie angezeigt werden.

Sollten für eine Terminologie keine alten Versionen vorhanden sein, ist die Tabelle leer.

In der Darstellung ist eine alte Version durch folgenden Banner erkennbar. Über diesen kann auch direkt zur aktuellen Version gewechselt werden.

![old-version-banner](old-version-banner.png "Banner"){: style="width: 100%"}

### Downloadformate

Folgende Downloadformate werden aktuell angeboten:

| Name | Dateiendung | Hinweis |
| ----------- | ----------- | ----------- |
| FHIR R4 xml | `.4.fhir.xml` | |
| FHIR R4 json | `.4.fhir.json` | |
| fsh v1 | `.1.fsh` | |
| fsh v2 | `.2.fsh` | |
| ClaML v2 | `.2.claml.xml` | |
| ClaML v3 | `.3.claml.xml` | |
| propCSV v1 | `.1.propcsv.csv` | |
| SVSextELGA v1 | `.1.svsextelga.xml` | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |
| outdatedCSV v1 | `.1.outdatedcsv.csv` | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |

#### FHIR R4 XML
Die XML-Darstellung für eine Ressource wird in diesem Format beschrieben [^6]:

     <name xmlns="http://hl7.org/fhir" (attrA="value")>
       <!-- from Resource: id, meta, implicitRules, and language -->
       <nameA><!--  1..1 type description of content  --><nameA>
       <nameB[x]><!-- 0..1 type1|type1 description  --></nameB[x]>
       <nameC> <!--  1..* -->
         <nameD><!-- 1..1 type>Relevant elements  --></nameD>
       </nameC>
     <name>

#### FHIR R4 JSON
Die JSON-Darstellung für eine Ressource basiert auf dem [JSON-Format, beschrieben in STD 90 (RFC 8259)](https://www.rfc-editor.org/info/std90) und wird unter Verwendung dieses Formats beschrieben [^7]:

    {
      "resourceType" : "[Resource Type]",
      // from Source: property0
      "property1" : "<[primitive]>", // short description
      "property2" : { [Data Type] }, // short description
      "property3" : { // Short Description
        "propertyA" : { CodeableConcept }, // Short Description (Example)
      },
      "property4" : [{ // Short Description
        "propertyB" : { Reference(ResourceType) } // R!  Short Description
      }]
    }

#### fsh v1 und v2
FHIR Shorthand (fsh) ist eine domänenspezifische Sprache zur Definition des Inhalts von FHIR Implementation Guides (IG). Die Sprache wurde speziell für diesen Zweck entwickelt, ist einfach und kompakt und ermöglicht es dem Autor, seine Absicht mit weniger Bedenken bezüglich der zugrunde liegenden FHIR-Mechanik auszudrücken. fsh kann mit einem beliebigen Texteditor erstellt und aktualisiert werden. Da es sich um Text handelt, ermöglicht es eine verteilte, teambasierte Entwicklung mit Quellcode-Kontrolltools wie GitHub [^5].


#### Proprietäres CSV
Das proprietäre CSV dient den Terminologieverwaltern zur leichter Bearbeitung von Terminologien. Dieses Format besteht aus einer Reihen von FHIR Metadaten-Elementen und Konzept-Elementen. In der FHIR Spezifikation sind die verpflichtenden Elemente für Codesysteme (http://hl7.org/fhir/codesystem.html#resource) und Value Sets (http://hl7.org/fhir/valueset.html#resource) definiert - auch die Bezeichnungen sind bereits vordefiniert und dürfen nicht verändert werden.

Der Wert der Bezeichnung wird in den jeweiligen FHIR-Datentypen (uri, Identifier, string, code, ...) konvertiert und abgespeichert, falls die Konvertierung erfolgreich war. Es können keine weiteren Metadaten-Elemente oder Konzept-Elemente angeführt werden, außer es wird eine eigene Extension für CodeSystem/ValueSet erstellt.

Die Metadaten-Elemente und Konzepte-Elemente können in jeglicher Reihenfolge angegeben werden. Es wird nach dem key-value-prinzip immer der Key ausgelesen, welcher dem darunter stehenden Value die Bedeutung gibt.

Folgende Metadaten-Elemente sind für die **ELGA Codesysteme** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this code system, represented as a URI (globally unique) (Coding.system) |
| identifier | 0..* | Additional identifier for the code system (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| content | **1..1** | not-present, example, fragment, complete, supplement |
| copyright | 0..1 | Use and/or publishing restrictions |

Folgende Konzept-Elemente sind für die **ELGA Codesysteme** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| code | **1..1** | Code that identifies concept |
| display | 0..1 | Text to display to the user |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | **1..1** | The text value for this designation |
| definition | 0..1 | Formal definition |
| property | 0..* | Property value for the concept - both attributes are combined by a pipe in the import file e.g. child\|101 |
| * code | 1..1 | Reference to CodeSystem.property.code |
| * value | 1..1 | Value of the property for this concept |

Folgende Metadaten-Elemente sind für die **ELGA Value Sets** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this value set, represented as a URI (globally unique) |
| identifier | 0..* | Additional identifier for the value set (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| copyright | 0..1 | Use and/or publishing restrictions |

Folgende Konzept-Elemente sind für die **ELGA Value Sets** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| system | 0..1 | The system the codes come from e.g. urn:oid:1.34.2.0.12.56.35 |
| code | **1..1** | Code or expression from system |
| display | 0..1 | Text to display for this code for this value set in this valueset |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | 1..1 | The text value for this designation |
| filter | 0..* | Select codes/concepts by their properties (including relationships) - all three attributes are combined by a pipe in the import file e.g. child\|not-in\|101 |
| * property | 1..1 | A property/filter defined by the code system |
| * op | 1..1 | =, is-a, descendent-of, is-not-a, regex, in, not-in, generalizes, exists |
| * value | 1..1 | Code from the system, or regex criteria, or boolean value for exists |
| exclude | 0..1 | Explicitly exclude codes from a code system or other value sets - means that this concept should be excluded (false/0 oder true/1) |


#### ClaML v2 und v3
ClaML (Classification Markup Language) ist ein spezielles XML-Datenformat für Klassifikationen. Der ClaML Export steht nur für Codelisten zur Verfügung. Informationen zur Terminologie bzw. Versionen sind in den ersten Elementen enthalten. Die OID findet sich im „uid“-Attribut des Elements Identifier. Weitere Informationen zur Terminologie finden sich in den ersten „Meta“-Elementen als name-value Paar:
Description: deutsche Beschreibung der Terminologie
* Description_eng: englische Beschreibung der Terminologie
* Website: Link zu Quelle etc.
* version_description: Beschreibung der Version
* insert_ts: Datum des Imports/Anlegen am Terminologieserver
* status_date: „Status geändert am“-Datum
* expiration_date: „gültig-bis“ Datum
* last_change_date: „geändert am“ Datum
* gueltigkeitsbereich
* statusCode: 0=>Vorschlag; 1=>Public; 2=>Obsolet
* unvollständig (bei Codelisten): Flag zur Kennzeichnung von Codelisten, die nur auszugsweise am Terminologieserver veröffentlicht sind
* verantw_Org (bei Codelisten): verantwortliche Organisation

Alle anderen Informationen zur Version befinden im title-Element:
* tite@date: „gültig-ab“ Datum
* tite@name: Name der Terminologie
* tite@version: Bezeichnung bzw. Nummer der Version

Die einzelnen Konzepte werden als class-Element abgebildet, jeweils mit name-value
Paaren:
* class/@code: Code
* \<Rubric kind='preferred'>/Label: Begriff
* \<Rubric kind='note'>/Label: nähere Beschreibung (Anwendungsbeschreibung) des Konzepts
* TS_ATTRIBUTE_HINTS: Hinweise zur Anwendung des Konzepts
* TS_ATTRIBUTE_MEANING: deutsche Sprachvariante, Bedeutung
* TS_ATTRIBUTE_STATUS/ TS_ATTRIBUTE_STATUSUPDATE: Informationen zum Status des einzelnen Konzepts
* Level/Type: Abbildung der Hierarchie
* Relationships: etwaige Verbindungen zu anderen Konzepten

Zu beachten ist hier, dass die Elemente in der Exportdatei nicht vorhanden sind, wenn sie am Terminologieserver nicht befüllt sind.

#### SVSextELGA
Das angebotene SVS ist ein erweitertes IHE Sharing Value Set Format. In diesem XML Format können Codelisten und Value Sets exportiert werden.
Diese enthält Attribute zur Terminologie bzw. der Version (Element „valueSet“):
* Name: Bezeichnung der Terminologie
* Beschreibung und description der Terminologie
* effectiveDate: „gültig-ab“ Datum der Version
* Id: OID der Version
* statusCode (derzeit nicht unterstützt)
* website: Link zu Quelle etc.
* version: Nummer/Bezeichnung der Version
* version-beschreibung (bei Codelisten): Beschreibung der Version
* gueltigkeitsbereich
* statusCode: 0=>Vorschlag: 1=>Public; 2=>Obsolet
* last_change_date: Datum der letzten Änderung

Alle Konzepte sind als „concept“-Elemente in der „conceptList“ vorhanden und enthalten folgende Attribute:
* code
* codeSystem: OID des Quell-Code Systems des Konzepts
* displayName: Begriff
* concept_beschreibung: nähere Beschreibung (Anwendungsbeschreibung) des Konzepts
* deutsch: deutsche Sprachvariante, Bedeutung
* einheit_codiert, einheit_print: nur relevant bei Laborparametern
* hinweise: Hinweise zur Anwendung des Konzepts
* level/type: Abbildung der Hierarchie
* relationships: etwaige Verbindungen zu anderen Konzepten
* orderNumber (nur bei Value Sets): Index zur Fixierung der Reihenfolge
* conceptStatus: 0=>Vorschlag; 1=>Public; 2=>Obsolet
* unvollständig (bei Codelisten): Flag zur Kennzeichnung von Codelisten, die nur auszugsweise am Terminologieserver veröffentlicht sind
* verantw_Org (bei Codelisten): verantwortliche Organisation

####outdatedcsv
Das Format "outdatedcsv" bildet das csv Exportformat des bisherigen Terminologieservers (termpub.gv.at) ab. Das Format enthält keine Metadaten zu Codesystemen oder Value Sets ab und kann auch nicht alle für FHIR erforderlichen Informationen abbilden, die in anderen Formaten vorhanden sind und ist somit nicht FHIR tauglich.
Zu jedem Konzept können in "outdatedcsv" lediglich folgende Informationen dargestellt werden:
* code
* codeSystem
* displayName
* parentCodeSystemName
* concept_Beschreibung
* meaning
* hints
* orderNumber
* level
* type
* relationships
* einheit print
* einheit codiert

**Dieses Format soll den Umstieg auf den neuen Terminologieserver (termgit.gv.at) erleichtern. Von einer weiteren Verwendung wird explizit abgeraten, da dieses Format nicht mehr weiterentwickelt wird.**

### Technologiebasis

Technologisch basiert der österreichische e-Health Terminologie-Browser auf TerminoloGit. Die folgenden zwei Projekte dienen der Weiterentwicklung der Prozesse und der Software, die für die Bereitstellung eines Terminologie-Browsers erforderlich sind.

| Projektname | Beschreibung | Repository-URL | GitLab-Projekt-ID | GitLab-Pages-URL |
| --- | --- | --- | --- | --- |
| TerminoloGit Dev | [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) | Repository für die technische Weiterentwicklung von TerminoloGit.<br/><br/>*Hinweis:* Unter dem angegebenen GitLab-Pages-URL werden die Inhalte der letzten erfolgreichen Pipeline eines beliebigen Git-Branches dieses Repositories dargestellt. | 21743825 | [https://elga-gmbh.gitlab.io/termgit-dev/](https://elga-gmbh.gitlab.io/termgit-dev/) |
| TerminoloGit Dev HTML | [https://gitlab.com/elga-gmbh/terminologit-dev-html](https://gitlab.com/elga-gmbh/terminologit-dev-html) | Repository für die statischen HTML-Seiten, die vom HL7® FHIR® IG Publisher auf Basis des `master`-Branches von [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) erstellt wurden. | 28239847 | [https://dev.termgit.elga.gv.at](https://dev.termgit.elga.gv.at) |

### Support und Helpdesk

#### Support für Implementierer
Verbesserungsvorschläge oder Fehlermeldungen, die bei der Anwendung des Systems erkannt wurden, können über das GitLab-Ticketsystem unter [https://gitlab.com/elga-gmbh/termgit/-/issues/new](https://gitlab.com/elga-gmbh/termgit/-/issues/new) eingemeldet werden.

#### Support für Terminologieuser
Für etwaige Anfragen zu den Terminologien steht das ELGA-Jira Kundenportal ([https://jira-neu.elga.gv.at/servicedesk/customer/portal/3](https://jira-neu.elga.gv.at/servicedesk/customer/portal/3)) zur Verfügung. Hier ist eine kurze kostenlose Selbstregistrierung erforderlich.

Außerdem besteht weiterhin die Möglichkeit Fragen über das E-Mailpostfach [cda@elga.gv.at](mailto:cda@elga.gv.at) einzubringen.

### Referenzen

[^1]:Concept Permanence, beschrieben in „Cimino’s Desiderata“ (Desiderata for Controlled Medical Vocabularies in the TwentyFirst Century; 1998; [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/))
[^2]: Mit wenigen Ausnahmen
[^3]: Ausnahmen sind statiche Value Set Bindings in CDA-Leitfäden („STATIC“), die sich explizit auf eine bestimmte Version eines Value Sets beziehen. Wenn nicht anders angegeben sind aber alle Bindings dynamisch, beziehen sich also immer auf die aktuelle Version.
[^4]:  Ein „CDA Beirat“, der Entscheidungen in Umlaufverfahren (qui tacet consentit) trifft. Die getroffenen Entscheidungen können per Mailverteiler publik gemacht werden.
[^5]: FHIR Short Hand, beschrieben in [http://hl7.org/fhir/uv/shorthand/2020May/](http://hl7.org/fhir/uv/shorthand/2020May/)
[^6]: FHIR XML, beschrieben in [http://www.hl7.org/fhir/xml.html](http://www.hl7.org/fhir/xml.html)
[^7]: FHIR JSON, beschrieben in [http://www.hl7.org/fhir/json.html](http://www.hl7.org/fhir/json.html)
