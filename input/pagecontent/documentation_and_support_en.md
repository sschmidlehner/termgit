> **Für die deutsche Version bitte [hier](documentation_and_support_de.html) klicken.**

### ELGA-Terminology

#### Codesystems
A large number of different code systems with different structures from various international, national and local sources are used in ELGA. These are brought into as uniform a form as possible via the terminology server.

The updating of code systems and their timing or frequency is the responsibility of the creators and thus mostly outside the sphere of influence of ELGA.

A code system is uniquely identified by an OID. The meaning of a code can only be decoded together with the OID of the code system.

A principle of terminology development is that codes must not be deleted and also the meaning of a code must not be changed [^1]. Such changes create an inconsistency in the use of older coded data with the current version of the code system. Usually, such "incompatible" versions of the code system are given different OIDs to keep them apart (e.g. the annual versions of ICD-10). Not all code systems adhere to this rule in practice. Therefore, care should always be taken when adopting updates of code systems.

#### Value Set
Wherever a value selection can be made by ELGA CDA implementation guides, a suitable Value Set is defined and specified with its unique name. ELGA Value Sets always refer [^2] to a specific "version" of a code system or even several code systems, which can be recognised via the OID of the code systems. This makes the Value Sets independent of changes to the code systems.

All Value Sets used in the implementation guides are published on the [Austrian e-Health Terminology Browser](https://termgit.elga.gv.at/).

Value Sets are not only identified by a unique name, but also by an OID and a version number, plus a validity date ("valid from"). In addition, Value Sets can contain a sequence and a tree structure (hierarchy).

Value Sets are valid until the validity date of a newer version of this Value Set is reached - then the newer version applies [^3]. As long as no newer version is available, a Value Set remains valid (see also chapter [Value Set Binding](#value-set-binding)).

Value Sets do not require status information for the individual codes, because by definition only "valid and usable" codes are contained in a Value Set.
If codes are no longer allowed to be used after a certain validity date, a new version of the Value Set is created from which the codes are removed. The new version of the Value Set is then given the new "valid from" date.

##### Value Set Binding
For each Value Set, a date is given when it becomes valid ("valid from" = = ValueSet.date). This is particularly important for Value Sets that are published before they come into validity.

For ELGA, a DYNAMIC binding to Value Sets applies. This means that the version of a Value Set currently published on the terminology server must always be applied. (Setting the corresponding keyword `DYNAMIC` is therefore not necessary in the guidelines).

Value Sets can also be STATICALLY bound to a code element. This is indicated by specifying the Value Set with name, OID, version and "valid from" date (= ValueSet.date) as well as the keyword `STATIC`.

##### Modifiability of Value Sets
Contents of Value Sets can change, but the name and OID of a Value Set remain the same. For new versions, the version number, date of change and "valid from" date (= ValueSet.date) are indicated. This allows the validity to be reconstructed at a specific time.

In exceptions, when defining a Value Set (in the Guidelines), it can be specified that it may not be changed or versioned ("Immutability" property = FHIR Value Set "immutable").

How often Value Sets are changed depends on the application. Value Sets needed for "structural" elements (e.g. in the CDA header, XDS metadata) will rarely change. "Content" Value Sets (e.g. for drug lists, laboratory analyses, diagnosis codes) will have to change correspondingly frequently; here, annual to weekly changes are to be expected.

#### Communication of changes to terminologies
All changes to terminologies can be accessed centrally on the terminology server. Checks for changed terminologies should take place at least **weekly**.

#### Requesting extensions or corrections to terminologies
All questions regarding ELGA-relevant terminologies can be reported to [cda@elga.gv.at](mailto:cda@elga.gv.at), from which the processing is centrally coordinated.

Code systems can only be corrected if (1) there is a technical problem or (2) it is a code system developed on behalf of ELGA GmbH.

Value sets defined by and for ELGA can be corrected after technical review by the content administrators. In the case of ELGA Value Sets that are subject to a regular maintenance and update cycle, extensions and changes can be made by the responsible content administrator (e.g. ELGA laboratory parameters, ASP list). In the case of other corrections to the content of ELGA Value Sets, the technical decision of the working group [^4] that developed the corresponding guideline within the framework of which the Value Set was defined may have to be obtained.

### Installation instructions
Work in progress

### Architecture
Work in progress

### GitLab Project Overview

The content of the [Austrian e-Health Terminology Browser](https://termgit.elga.gv.at/) is based on the following two GitLab projects:

| Project Name | Description | Repository URL | GitLab Project ID | GitLab Pages URL |
| --- | --- | --- | --- | --- |
| TerminoloGit | [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit) | Repository for published, up-to-date terminologies (download formats).<br/><br/>*Note:* The contents of the last successful pipeline of any Git branch of this repository are displayed under the specified GitLab Pages URL. | 33179072 | [https://elga-gmbh.gitlab.io/termgit](https://elga-gmbh.gitlab.io/termgit) |
| TerminoloGit HTML | [https://gitlab.com/elga-gmbh/termgit-html](https://gitlab.com/elga-gmbh/termgit-html) | Repository for the static HTML pages created by the HL7® FHIR® IG Publisher based on the `main` branch of [https://gitlab.com/elga-gmbh/termgit](https://gitlab.com/elga-gmbh/termgit). | 33179017 | **[https://termgit.elga.gv.at](https://termgit.elga.gv.at)** |

### Release process
1. Status "draft": Only certain users may create, change or import terminologies. Each terminology has a terminology owner, terminology administrators can change all terminologies. All newly created or newly imported terminologies or terminology versions have the status "draft", which makes them visible only to the assigned terminology manager and all terminology administrators.
2. Status "for approval": The terminology manager approves the content of the terminology version, the terminology thereby assumes this new status (still only visible to the assigned terminology manager and terminology administrators).
3. Status "approved ": The terminologies created or changed are also available to terminology consumers after approval by a terminology administrator.
4. Status "invalid": These terminologies may not be used (any more). This can have two reasons:
   1. The draft created was rejected by the terminology administrator.
   2. The assigned terminology manager or a terminology administrator withdraws the version and manually sets it to invalid.

![Release process](Freigabeprozess_en.png "Release process"){: style="width: 100%"}

The status always refers to the terminology version, not to individual concepts. However, the terminology (one level above) must evolve with the status of the versions to guarantee correct visibility and exportability. Example:
* A terminology is created by importing an initial version: Terminology and version are in draft status.
* The only version (or all versions) of a terminology are set to invalid: The terminology must also assume the status "invalid".

The attributes mentioned in the figure are not to be understood as additional status attributes of a terminology version. They are only a status and result from other attributes. Details:
* "visible": Visible for all terminology consumers by default.
  * The version is exportable for all, without its own request parameter
  * Can be derived from version status and validity date
* "last valid": most recent version for which the validity date has been reached
  * Results from validity date and (non-)existence of a newer latest version
    * Should be available as a "calculated" request parameter in the web service export.

### Publication process for terminologies from external publishers

The following process is defined for the publication of terminologies created by ELGA-external terminology developers for the Austrian e-Health Terminology Server.

#### Requirements

- Basic knowledge of Git and GitLab respectively are assumed to be known.
- The desired terminology is not subject to any external licensing requirements and already has its own OID. If there is no OID for the terminology yet, it can be requested via the [OID platform](https://www.gesundheit.gv.at/OID_Frontend/application.htm?section=5).

#### Process

1. Contact ELGA GmbH via e-mail address [cda@elga.gv.at](mailto:cda@elga.gv.at) with **[completed application form](files/Applicationform_external_terminology_developer_de.pdf)** (currently available in German only):
   1. Name of the terminology
   2. OID of the terminology
   3. Code system/value set
      1. In case of a value set: disclosure of the associated code system(s)
   4. Description of the terminology (DE and EN)
   5. Person and organization responsible for the terminology
      1. Name (organization)
      2. E-mail address (organization)
      3. Name (responsible person)
      4. E-mail address (responsible person)
   6. GitLab.com user who is to contribute the terminology to the GitLab project [TerminoloGit](https://gitlab.com/elga-gmbh/termgit). The nomination of more than one GitLab.com user is possible.
2. The completed form will be checked technically and legally by ELGA GmbH.
3. After successful verification, "Developer" rights will be granted to the GitLab users named by the applicant in the GitLab project [TerminoloGit](https://gitlab.com/elga-gmbh/termgit). This allows the user to create branches, but not to merge into the default branch of the project without approval by a user with "Maintainer" rights.
4. For the initial creation of the import file, **import templates** for **[code system](files/cs_import_template.csv)** or **[value set](files/vs_import_template.csv)** are provided in the form of CSV files. The required attributes are described in the section [Proprietary CSV](#proprietary-csv).
   1. Uploading to the GitLab project can be done via different ways (e.g. GitLab WebIDE or software like Sourcetree). For questions and support, the external terminology developer can contact the TerminoloGit team ([cda@elga.gv.at](mailto:cda@elga.gv.at)).
   2. Thematically related terminologies should be edited together in one branch to maintain clarity.
   3. Commits must comply with Git governance: [https://cbea.ms/git-commit/](https://cbea.ms/git-commit/)
   4. Terminologies from external terminology developers are collected under a separate category named after the organization name.
5. After completion of the terminology, a merge request is created so that the branch can be merged into the default branch.
   1. The technical review is performed by the ELGA GmbH.
   2. If the technical requirements are met, the branch is merged to the default branch.
6. After the pipelines have been successfully run, a check is made to ensure that the desired terminologies have been published correctly and completely.
7. Finally, the terminology developer is informed about the publication and an update of the [Semantic Competence Center changelist](https://confluence.elga.gv.at/display/SCCTERM/Terminologien+-+Changelist+2022) is published.

### Usecases/Usages and Web Services

#### Browsing/Research
*Actor:* Student

The browser is ideal for research purposes or simply for browsing, as all lists are presented in a well-structured HTML page. The user has the possibility to call up different code systems and value sets individually in a structured form or to search for specific concepts with the help of the search function. There is always a link between the code systems and value sets to make it easier to find lists that belong together.

#### Automatic import of catalogues
*Actor:* System

To ensure that your systems are always up to date, there are different options of automatically importing catalogues. There are several ways to ensure that you always have the latest lists in your system. The variants presented here focus on updating a specific terminology.

Basically, the following URL template can always be used to retrieve the latest version of a terminology:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/<NAME_OF_TERMINOLOGY>/<NAME_OF_TERMINOLOGY><FORMAT>

Here the
- `<NAME_OF_TERMINOLOGY>` corresponds to the type of the terminology (`CodeSystem` or `ValueSet`) and the name of the terminology (e.g. `iso-3166-1-alpha-3`). The type and name are linked with `-` resulting in `CodeSystem-iso-3166-1-alpha-3`.
- `<FORMAT>` corresponds to the file extension of one of the offered formats (see [Download Formats](#download)), e.g. `.4.fhir.xml`

The result then looks like this:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.4.fhir.xml

Based on a particular format of a terminology, different types of retrieval are presented in the next points:
1. As a stable https link to the file, without versioning details:
   * Python:

         response = urllib2.urlopen('https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false')
   * Java:

         URL url = new URL("https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false");
         URLConnection connection = url.openConnection();
         InputStream is = connection.getInputStream();
         // .. then download the file
2. With git via ssh within a local repository and the Git-tags for the corresponding terminology:
   * First clone of the repository:

         git clone git@gitlab.com:elga-gmbh/termgit.git

   * Update of the local Git repository (incl. Git-tags):

         git fetch --tags -f

   * Checking the difference between current local directory content and that of the Git-tag of a terminology once the repository has been updated:

         git log HEAD..tags/CodeSytem-iso-3166-1-alpha-3

     or

         git log HEAD..tags/1.0.3166.1.2.3

   * Checkout the latest version of a terminology with the corresponding Git-tag. **Note:** This will set the entire repository to the state of the Git-tag. Other terminologies might have a more recent state.

         git checkout tags/CodeSystem-iso-3166-1-alpha-3

     or

         git checkout tags/1.0.3166.1.2.3

3. With git via ssh without a local repository and the Git-tags for the corresponding terminology-folder (no support for receiving specific files):

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git CodeSystem-iso-3166-1-alpha-3:terminologies/CodeSystem-iso-3166-1-alpha-3

   or

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git 1.0.3166.1.2.3:terminologies/CodeSystem-iso-3166-1-alpha-3

4. With GitLab API via REST for a specific format (here .2.claml.xml; '/' needs to be escaped through '%2f')

       curl https://gitlab.com/api/v4/projects/33179072/repository/files/terminologies%2fCodeSystem-iso-3166-1-alpha-3%2fCodeSystem-iso-3166-1-alpha-3.2.claml.xml?ref=main

5. With FHIR via REST (FHIR Server comes with alpha release in Q3 2022):
   * curl:

         curl https://<FHIR SERVER NAME>.at/CodeSystem/CodeSystem-iso-3166-1-alpha-3

   * for more details see [http://www.hl7.org/fhir/overview-dev.html#Interactions](http://www.hl7.org/fhir/overview-dev.html#Interactions)
   * for testing purposes you can use, e.g.:

         curl http://hapi.fhir.org/baseR4/CodeSystem/2559241

##### Retrieving meta information of a Git-tag.

The [GitLab API for Git Tags](https://docs.gitlab.com/ee/api/tags.html) can be used for automated checking whether a particular terminology has been updated. The following command can be used to retrieve the metadata of a Git-tag:

    curl https://gitlab.com/api/v4/projects/33179072/repository/tags/CodeSystem-iso-3166-1-alpha-3

Among other things, the metadata includes the date the Git-tag was created (`created_at`).

#### Manual updating of terminologies
*Actor:* Master data/system admin

In addition to the automatic import of catalogues, it is also possible to manually update all the terminologies. For this, we recommend activating the tag news in GitLab. You will then receive an automatic notification for each change.

![subscribe-tag](Tag-abonnieren.png "subscribe-tag"){: style="width: 100%"}

![manual-mapping](automatische-benachrichtigung_en.png "manual-mapping"){: style="width: 100%"}

#### Validation of codes
*Actor:* System

The system can also use the new Austrian e-Health Terminology Server to check whether a particular code is part of a code system or value set. This works with the FHIR operation [`$validate-code` for Code Systems](http://www.hl7.org/fhir/codesystem-operation-validate-code.html) and [`$validate-code` for Value Sets](http://www.hl7.org/fhir/valueset-operation-validate-code.html).

![validate-code](validate-code.png "Validate_code"){: style="width: 100%"}

### Roles
As a basic rule, the terminology server is a system for the provision of code systems and value sets, whereby in principle no real-time access is provided during processing by the primary systems (e.g.: Health Service Provider Information Systems (GDA)) is provided. The terminology server is not currently used as an online resource (for checking individual values), but as a source for new terminologies to be stored locally.
The terminology server is used by the following user groups:

| User group | Task | GitLab.com-Role | Description |
| --- | --- | --- | ---|
| Terminology consumer | read-only access | none, not required | As terminology consumer no self-registration is required for viewing and exporting terminologies or checking for new versions. |
| Terminology maintainer | Terminology maintenance | Developer | Terminologies are created or updated by the terminology maintainer. It is not possible to assign a responsible user to each terminology. |
| Terminology administrator | Release and publication | Maintainer | Before terminologies become publicly available, they are approved by a terminology administrator. |

### Versioning
The following version scheme is used for code systems and value sets:

    MAJOR.MINOR.PATCH+YYYYMMDD

* `MAJOR` is increased if e.g.
    * a concept is added (the value set changes and must be updated by the implementor in his implementation)
    * a concept is set deprecated/obsolete;
    * the DisplayName of a concept is changed;
    * metadata of a concept changes and thus the meaning of the concept changes;
    * structure/grouping (e.g. ValueSet) changes;
* `MINOR` always remains at the value 0 and is not used in the context of terminologies
* `PATCH` is increased if e.g.
    * Spelling errors or typos are corrected in the metadata (not DisplayName or Code) of an existing concept.
* `YYYYMMDD` corresponds to the date when the terminology was updated.

#### Display previous version
Previous versions of a terminology can be accessed by clicking the "Previous Versions" tab. Subsequently, a table comprising of the following columns will be displayed:

- `Publication Date`: This column displays date when the terminology has been published. By clicking the date the old version of the terminology can be displayed. Note, that the publication date can be different from the terminology's `valid from` date.
- `Current Version vs. Outdated Version`: Using the [W3C HTML diff tool](https://services.w3.org/htmldiff) the differences between the outdated and the current version can be inspected.

If there exist no previous version for a terminology, the table will be empty.

Old versions are recognizable by the following banner. It also provides a link to the latest version of the terminology.

![old-version-banner](old-version-banner.png "Banner"){: style="width: 100%"}

### Download

The following download formats are currently offered:

| Name | File extension | Note |
| ----------- | ----------- | ----------- |
| FHIR R4 xml | `.4.fhir.xml` | |
| FHIR R4 json | `.4.fhir.json` | |
| fsh v1 | `.1.fsh` | |
| fsh v2 | `.2.fsh` | |
| ClaML v2 | `.2.claml.xml` | |
| ClaML v3 | `.3.claml.xml` | |
| propCSV v1 | `.1.propcsv.csv` | |
| SVSextELGA v1 | `.1.svsextelga.xml` | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |
| outdatedCSV v1 | `.1.outdatedcsv.csv` | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |

#### FHIR R4 XML
The XML representation for a resource is described using this format [^6]:

     <name xmlns="http://hl7.org/fhir" (attrA="value")>
       <!-- from Resource: id, meta, implicitRules, and language -->
       <nameA><!--  1..1 type description of content  --><nameA>
       <nameB[x]><!-- 0..1 type1|type1 description  --></nameB[x]>
       <nameC> <!--  1..* -->
         <nameD><!-- 1..1 type>Relevant elements  --></nameD>
       </nameC>
     <name>

#### FHIR R4 JSON
The JSON representation for a resource is based on the [JSON format described in STD 90 (RFC 8259)](https://www.rfc-editor.org/info/std90) , and is described using this format [^7]:

    {
      "resourceType" : "[Resource Type]",
      // from Source: property0
      "property1" : "<[primitive]>", // short description
      "property2" : { [Data Type] }, // short description
      "property3" : { // Short Description
        "propertyA" : { CodeableConcept }, // Short Description (Example)
      },
      "property4" : [{ // Short Description
        "propertyB" : { Reference(ResourceType) } // R!  Short Description
      }]
    }

#### fsh v1 and v2
FHIR Shorthand (FSH) is a domain-specific language for defining the contents of FHIR Implementation Guides (IG). The language is specifically designed for this purpose, simple and compact, and allows the author to express their intent with fewer concerns about underlying FHIR mechanics. FSH can be created and updated using any text editor, and because it is text, it enables distributed, team-based development using source code control tools such as GitHub [^5].

#### Proprietary CSV
The proprietary CSV is used by terminology administrators to easily edit terminologies. This format is a set of FHIR metadata elements and concept elements. The FHIR specification defines the mandatory elements for code systems (http://hl7.org/fhir/codesystem.html#resource) and value sets (http://hl7.org/fhir/valueset.html#resource) - the designations are also predefined and may not be changed.

The value of the designation is converted into the respective FHIR data types (uri, identifier, string, code, ...) and stored if the conversion was successful. No other metadata elements or concept elements can be quoted unless a custom extension is created for CodeSystem/ValueSet.

The metadata elements and concept elements can be specified in any order. According to the key-value-principle always the key is read, which gives the meaning to the value standing under it.

The following metadata elements are desired for the **ELGA code systems**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this code system, represented as a URI (globally unique) (Coding.system) |
| identifier | 0..* | Additional identifier for the code system (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| content | **1..1** | not-present, example, fragment, complete, supplement |
| copyright | 0..1 | Use and/or publishing restrictions |

The following concept elements are desired for the **ELGA code systems**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| code | **1..1** | Code that identifies concept |
| display | 0..1 | Text to display to the user |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | **1..1** | The text value for this designation |
| definition | 0..1 | Formal definition |
| property | 0..* | Property value for the concept - both attributes are combined by a pipe in the import file e.g. child\|101 |
| * code | 1..1 | Reference to CodeSystem.property.code |
| * value | 1..1 | Value of the property for this concept |

The following metadata elements are desired for the **ELGA value sets**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this value set, represented as a URI (globally unique) |
| identifier | 0..* | Additional identifier for the value set (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| copyright | 0..1 | Use and/or publishing restrictions |

The following concept elements are desired for the **ELGA value sets**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| system | 0..1 | The system the codes come from e.g. urn:oid:1.34.2.0.12.56.35 |
| code | **1..1** | Code or expression from system |
| display | 0..1 | Text to display for this code for this value set in this valueset |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | 1..1 | The text value for this designation |
| filter | 0..* | Select codes/concepts by their properties (including relationships) - all three attributes are combined by a pipe in the import file e.g. child\|not-in\|101 |
| * property | 1..1 | A property/filter defined by the code system |
| * op | 1..1 | =, is-a, descendent-of, is-not-a, regex, in, not-in, generalizes, exists |
| * value | 1..1 | Code from the system, or regex criteria, or boolean value for exists |
| exclude | 0..1 | Explicitly exclude codes from a code system or other value sets - means that this concept should be excluded (false/0 oder true/1) |


#### ClaML v2 and v3
ClaML (Classification Markup Language) is a special XML data format for classifications. The ClaML export is only available for code lists. Information on terminology or versions is contained in the first elements. The OID is found in the "uid" attribute of the Identifier element. Further information on the terminology can be found in the first "meta" elements as a name-value pair:
Description: German description of the terminology.
* Description_eng: English description of the terminology.
* Website: Link to source etc.
* version_description: description of the version
* insert_ts: date of import/creation on terminology server
* status_date: "status changed on" date
* expiration_date: "valid until" date
* last_change_date: "changed on" date
* gueltigkeitsbereich
* statusCode: 0=>Suggestion; 1=>Public; 2=>Obsolet
* unvollständig (for code lists): flag to indicate code lists that are only published in part on the terminology server
* verantw_Org (for code lists): responsible organisation

All other version information is in the title element:
* tite@date: "valid-from" date
* tite@name: name of the terminology
* tite@version: name or number of the version.

The individual concepts are mapped as class elements, each with name-value
pairs:
* class/@code: Code
* \<Rubric kind='preferred'>/Label: term
* \<Rubric kind='note'>/Label: more detailed description (application description) of the concept
* TS_ATTRIBUTE_HINTS: hints on the application of the concept
* TS_ATTRIBUTE_MEANING: German language variant, meaning
* TS_ATTRIBUTE_STATUS/ TS_ATTRIBUTE_STATUSUPDATE: Information on the status of the individual concept.
* Level/Type: mapping of the hierarchy
* Relationships: any connections to other concepts.

Note here that the elements are not present in the export file if they are not populated on the terminology server.

#### SVSextELGA
The SVS offered is an extended IHE Sharing Value Set format. In this XML format, code lists and value sets can be exported.
This contains attributes for the terminology or the version (element "valueSet"):
* Name: Designation of the terminology
* Description of the terminology
* effectiveDate: "valid-from" date of the version
* Id: OID of the version
* statusCode (currently not supported)
* website: Link to source etc.
* version: number/designation of the version
* version-description (for code lists): description of the version
* validity range
* statusCode: 0=>Suggestion: 1=>Public; 2=>Obsolet
* last_change_date: date of last change

All concepts are present as "concept" elements in the "conceptList" and contain the following attributes:
* code
* codeSystem: OID of the source code system of the concept
* displayName: term
* concept_beschreibung: more detailed description (application description) of the concept
* deutsch: German language variant, meaning
* einheit_codiert, einheit_print: only relevant for laboratory parameters
* hinweis: Notes on the application of the concept
* level/type: mapping of the hierarchy
* relationships: possible connections to other concepts
* orderNumber (only for value sets): index to fix the order
* conceptStatus: 0=>Suggestion; 1=>Public; 2=>Obsolet
* unvollständig (for code lists): flag to indicate code lists that are only published in excerpts on the terminology server
* verantw_Org (for code lists): responsible organisation

####outdatedcsv
The "outdatedcsv" format is the csv export format of the previous terminology server (termpub.gv.at). The format does not contain any metadata on code systems or value sets and cannot represent all the information required for FHIR that is available in other formats and is therefore not suitable for FHIR.
For each concept, only the following information can be represented in "outdatedcsv":
* code
* codeSystem
* displayName
* parentCodeSystemName
* concept_Beschreibung
* meaning
* hints
* orderNumber
* level
* type
* relationships
* einheit print
* einheit codiert

**This format is intended to facilitate the changeover to the new terminology server (termgit.gv.at). Further use is explicitly discouraged, as this format is no longer being developed.**

### Technological base

Technologically, the Austrian e-Health Terminology Browser is based on TerminoloGit. The following two projects are meant to develop the processes and software needed to provide a terminology browser.

| Project Name | Description | Repository URL | GitLab Project ID | GitLab Pages URL |
| --- | --- | --- | --- | --- |
| TerminoloGit Dev | [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) | Repository for TerminoloGit technical development.<br/><br/>*Note:* The contents of the last successful pipeline of any Git branch of this repository are displayed under the specified GitLab Pages URL. | 21743825 | [https://elga-gmbh.gitlab.io/termgit-dev/](https://elga-gmbh.gitlab.io/termgit-dev/) |
| TerminoloGit Dev HTML | [https://gitlab.com/elga-gmbh/terminologit-dev-html](https://gitlab.com/elga-gmbh/terminologit-dev-html) | Repository for the static HTML pages created by the HL7® FHIR® IG Publisher based on the `master` branch of [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev). | 28239847 | [https://dev.termgit.elga.gv.at](https://dev.termgit.elga.gv.at) |

### Support and helpdesk

#### Support for implementers
Suggestions for improvement or errors detected while using the system can be reported via the GitLab ticket system at [https://gitlab.com/elga-gmbh/termgit/-/issues/new](https://gitlab.com/elga-gmbh/termgit/-/issues/new).

#### Support for terminology users
The ELGA-Jira customer portal ([https://jira-neu.elga.gv.at/servicedesk/customer/portal/3](https://jira-neu.elga.gv.at/servicedesk/customer/portal/3)) is available for any queries about the terminologies. A short self-registration is required free of charge.

In addition, it is still possible to submit questions via the e-mail box [cda@elga.gv.at](mailto:cda@elga.gv.at).

### References

[^1]:Concept Permanence, beschrieben in „Cimino’s Desiderata“ (Desiderata for Controlled Medical Vocabularies in the TwentyFirst Century; 1998; [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3415631/))
[^2]: Mit wenigen Ausnahmen
[^3]: Ausnahmen sind statiche Value Set Bindings in CDA-Leitfäden („STATIC“), die sich explizit auf eine bestimmte Version eines Value Sets beziehen. Wenn nicht anders angegeben sind aber alle Bindings dynamisch, beziehen sich also immer auf die aktuelle Version.
[^4]:  Ein „CDA Beirat“, der Entscheidungen in Umlaufverfahren (qui tacet consentit) trifft. Die getroffenen Entscheidungen können per Mailverteiler publik gemacht werden.
[^5]: Fhir Short Hand, beschrieben in [http://hl7.org/fhir/uv/shorthand/2020May/](http://hl7.org/fhir/uv/shorthand/2020May/)
[^6]: FHIR XML, beschrieben in [http://www.hl7.org/fhir/xml.html](http://www.hl7.org/fhir/xml.html)
[^7]: FHIR JSON, beschrieben in [http://www.hl7.org/fhir/json.html](http://www.hl7.org/fhir/json.html)
