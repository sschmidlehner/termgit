> **Für die deutsche Version bitte [hier](faq_de.html) klicken.**

All information about the new terminology server and the use of terminologies is available in two languages under [Dokumentation und Support (de)](documentation_and_support_de.html) or [Documentation and Support (en)](documentation_and_support_en.html). The documentation is continuously updated and improved.

The most important questions and answers about the terminology server are summarized here.

### Where can I find code lists and value sets that I have used so far?
Via [Table of Contents](toc.html) and [Artifact Summary](artifacts.html), respectively, the terminologies can be retrieved. For easier readability, only the postfixes (e.g. "_CS" or "_VS") have been removed. As soon as you click on a terminology, the details, such as whether it is a code system or value set, are displayed in the summary.

Table of Contents lists all terminologies alphabetically. Under the Artifact Summary, all terminologies are grouped by code systems or value sets.

An advanced search is still to be implemented.

### What are the download formats? How do the new formats correspond to those of the old terminology server?

Currently, the following download formats are offered under the 'Download' tab of the respective terminology:

| *new* Terminologieserver | *old* Terminologieserver | Note |
| ----------- | ----------- | ----------- |
| FHIR R4 xml `.4.fhir.xml` | no correspondence       | |
| FHIR R4 json `.4.fhir.json`   | no correspondence        | |
| fsh v1 `.1.fsh`   | no correspondence        | |
| fsh v2 `.2.fsh`   | no correspondence        | |
| ClaML v2 `.2.claml.xml`   | ClaML        | |
| ClaML v3 `.3.claml.xml`   | no correspondence        | |
| propCSV v1 `.1.propcsv.csv`   | no correspondence        | |
| SVSextELGA v1 `.1.svsextelga.xml`   | SVS        | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |
| outdatedCSV v1 `.1.outdatedcsv.csv`   | CSV        | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |

A more detailed description of the respective download formats can be found under [download](documentation_and_support_en.html#download).

### How do I know which terminology is the current one?

The terminologies that can be displayed and accessed via [Table of Contents](toc.html) or [Artifact Summary](artifacts.html) always represent the most recent versions.

Table of Contents lists all terminologies alphabetically. Under the Artifact Summary, all terminologies are grouped according to code systems or value sets.

### Where can I find old versions?

In the new terminology server, all published versions can be called up for each terminology under the tab `Previous Versions`. When old versions are displayed, a warning is shown that this is an old version. This notice also includes a link to the current terminology version.

For parallel operation, initially only the most current versions of the terminologies were migrated to the new terminology server. Old versions can still be accessed at [https://termpub.gesundheit.gv.at/](https://termpub.gesundheit.gv.at/).

The old terminology server will be operated until October 2022.

A solution for making old terminology versions available on the new terminology server is still being worked on.

### How can I be notified of updates?

See [Automatic import of catalogues](documentation_and_support_en.html#automatic-import-of-catalogues) or [Manual updating of terminologies](documentation_and_support_en.html#manual-updating-of-terminologies).

### How to suggest changes or report bugs?

See [Support and helpdesk](documentation_and_support_en.html#support-and-helpdesk)

