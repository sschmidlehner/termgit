# TerminoloGit

[[_TOC_]]

> **For general documentation see [documentation and support](input/pagecontent/documentation_and_support.md)**

The whole concept is beeing displayed in a fully dressed use case diagram, including written subfunction/fish level use cases. Please note that this does not comply to the usual Cockburn's fully dressed use case diagram. There were made a few modifications to display all needed information in one diagram.

### Fully dressed use case diagram

![Fully dressed use case diagram](fully_dressed_use_case_diagram.png "Fully_dressed_use_case_diagram")

### Parameters for all use cases:

1. __Scope:__
    * a terminology like a codesystem or valueset

2. __Level:__
    * Subfunction/Fish

## 1. Create Account, Authenticate and Authorisate

1. __Actors:__
    * Terminology user (human)
    * GitLab Service (machine)

1. __Brief:__ The terminology user needs an account for special interactions like insight into SNOMED CT codes and creates one via GitLab.

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * if the user authorisation by ELGA GmbH is not done
            * waiting till the authorisation process is complete
    2. __Success Guarantees:__
        * A user account with individual access rights has been created.

1. __Preconditions:__
    * The GitLab project TerminoloGit has been created

1. __Triggers:__
    * A terminology user wants to perform a special interaction

1. __Basic flow:__
    1. Creating an account on GitLab
    1. Requesting individual permissions for the different GitLab repositories
    1. Login in GitLab

1. __Extensions:__ none

1. __Software:__
    * Any browser

1. __Hardware:__
    * Any client with a browser

1. __Services:__
    * GitLab.com or own GitLab CE

## 2. Subscription

1. __Actors:__
    * GitLab Service (machine)

1. __Brief:__ The subscribers are being informed per mail, because a new or edited terminology-file has been pushed.

1. __Postconditions:__
    1. __Minimal Guarantees:__ none
    2. __Success Guarantees:__
        * A subscriber is being informed of a new terminology

1. __Preconditions:__
    * The GitLab project TerminoloGit has been configured with subscribers

1. __Triggers:__
    * A terminology has been added or updated

1. __Basic flow:__
    1. GitLab sends the change to all subscribers

1. __Extensions:__ none

1. __Software:__
    * Any mail program

1. __Hardware:__
    * Any client with a mail program

1. __Services:__
    * GitLab for sending the change to subscribers

## 3. Using FHIR IG and FHIR-tx-at

1. __Actors:__
   * Terminology user (human or machine)
   * GitLab Service (machine)

1. __Brief:__ A terminology like a codesystem or valueset is being retrieved via the GUI or REST interface

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * if the terminology the user is looking for exists
            * receiving a terminology
    2. __Success Guarantees:__
        * receiving any feedback

1. __Preconditions:__
    * The TerminoloGit GitLab project exists
    * The FHIR IG is published
    * The FHIR-tx-at is up to date

1. __Triggers:__
    * A terminology needs to be retrieved

1. __Basic flow:__
    1. By requesting the FHIR IG web side you get a browsable format of all terminologies, with the possibility to download them in any format
    1. By operating with worldwide standardized FHIR ValueSet or CodeSystems operations the terminologies are retrieved machine-readable

1. __Extensions:__
    * A validation of concepts in ressources ValueSet or CodeSystem is possible by $validate-code.
    * A hierarchy status between two concepts in a CodeSystem is possible by $subsumes.
    * A detailed view of a concept in a CodeSystem is possible by $lookup.

1. __Software:__
    * A browser or rest-client

1. __Hardware:__
    * Client for browsing or sending rest-operations

1. __Services:__
    * GitLab for publishing terminologies
    * a server for rest-operations

## 4. CRUD and push a terminology to GitLab

1. __Actors:__
    * Terminology expert (human)
    * GitLab Service (machine)

1. __Brief:__ A new terminology-file is being created or edited and pushed to the GitLab.com project

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * If the commit succeeds (e.g. no merge conflicts):
            * The new, deleted or edited terminology-file is published in the TerminoloGit GitLab project and git-customary versioned
            * The continuous delivery is starting and triggering the next steps
    2. __Success Guarantees:__
        * By pushing a feedback is returning from the git-client

1. __Preconditions:__
    * The TerminoloGit GitLab project exists
    * A local git client and an editor is installed on a client

1. __Triggers:__
    * A terminology needs to be created or updated

1. __Basic flow:__
    1. With an editor like Notepad++ or VSC a terminology-file in the local git repository is being updated or created or a deleted by setting the status to retired.
    1. In your local git client a new commit is being created and pushed to the remote repository TerminoloGit

1. __Extensions:__
    * A branch beside of master could indicate that a new commit is pushed for testing or approval purposes. In this case the Continuous Delivery of GitLab is the same as for master, with the only difference that only development users can see the results. This should be accomplished by a login or an unknown url for the FHIR IG and FHIR-tx-at.

1. __Software:__
    * a local git client i.e. SourceTree (free git GUI)
    * an editor i.e. Notepad++ or Visual Studio Code (VSC) (free code editors)

1. __Hardware:__
    * Client for editing text files and git pushing

1. __Services:__
    * GitLab for publishing terminologies

## 5. Continuous Delivery to FHIR IG and FHIR-tx-at

1. __Actors:__
    * GitLab Service (machine)

1. __Brief:__ A new or edited terminology-file is being processed to other terminology classes from the GitLab service by a runner

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * If there is no convert conflict:
            * The new or edited terminology-files are being converted by MaLaC-CT into all the other classes
            * The FHIR IG is being built with links to all classes
            * The FHIR-tx-at is being updated with the new terminologies
    2. __Success Guarantees:__
        * A message is returning from the GitLab runner

1. __Preconditions:__
    * The TerminoloGit GitLab CI is configured

1. __Triggers:__
    * A terminology has been added or updated

1. __Basic flow:__
    1. a runner is being started because a new commit has been pushed
    1. the runner is converting the terminology to all other classes i.e. fsh, FHIR, ClaMl, SVS, the predefined CSV
    1. if the conversion passed, the IG Publisher is started
    1. if the IG Publisher passed, the upload to FHIR-tx-at is starting

1. __Extensions:__ none

1. __Software:__ none

1. __Hardware:__ none

1. __Services:__
    * GitLab for running the runner

## 6. Push to branches, approve and merge to master

1. __Actors:__
    * Terminology expert (human)
    * GitLab Service (machine)

1. __Brief:__ A new or edited terminology-files are being committed to a branch and requested for a merge to master.

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * If a correction is needed:
            * waiting that the committer corrects with a new commit
    2. __Success Guarantees:__
        * a branch has been merged to master

1. __Preconditions:__
    * The TerminoloGit project is configured for branching

1. __Triggers:__
    * A terminology has been added or updated to a branch

1. __Basic flow:__
    1. the terminology expert creates a commit with his changes
    1. the terminology expert pushes his commits to a branch
    1. the terminology expert requests a merge to master or another protected branch
    1. a terminology administrator comments the request and demands changes or accepts this branch and merges it into master

1. __Extensions:__ none

1. __Software:__
    * a browser

1. __Hardware:__
    * a machine with a browser

1. __Services:__
    * GitLab for running the runner

